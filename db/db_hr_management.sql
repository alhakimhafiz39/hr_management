CREATE DATABASE hr_management;

-- untuk genearate UUID()

CREATE OR REPLACE FUNCTION public.uuid(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
   vUUID VARCHAR(50);

   vReturn VARCHAR(32);

BEGIN
	select INTO vUUID uuid_generate_v4();
	vReturn := replace(vUUID,'-','');

	RETURN vReturn;

END

$BODY$;



CREATE OR REPLACE FUNCTION public.uuid_generate_v4(
	)
    RETURNS uuid
    LANGUAGE 'c'

    COST 1
    VOLATILE STRICT PARALLEL SAFE
AS '$libdir/uuid-ossp', 'uuid_generate_v4'
;


-- end untuk generate UUID()

-- untuk generate sequence user,menu dan role
CREATE SEQUENCE public.tbl_menu_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE public.tbl_user_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
-- end untuk generate sequence

CREATE table tbl_user (
    user_id integer NOT NULL DEFAULT nextval('tbl_user_id_seq'::regclass),
    username character varying (50),
    password character varying (100),
    role_id character varying(32),
    id_karyawan character varying(32),
    created_date timestamp without time zone,
    created_by integer,
    last_login timestamp without time zone
);

CREATE TABLE tbl_role(
    id character varying DEFAULT uuid(),
    role_name character varying(100),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_menu(
    menu_id integer NOT NULL DEFAULT nextval('tbl_menu_id_seq'::regclass),
    menu_title character varying(50),
    menu_url character varying(50),
    parent_id integer DEFAULT 0,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_user_nav(
    user_id integer,
    menu_id integer,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_calon_karyawan(
    id character varying(32) DEFAULT uuid(),
    kode_calon_karyawan character varying(10),
    nik character varying(20),
    nama character varying(100),
    jenis_kelamin character varying(1), -- P - PRIA -  W: WANITA
    tempat_lahir character varying(50),
    tanggal_lahir date,
    agama integer DEFAULT 0,
    alamat text,
    no_hp character varying(20),
    email character varying(50),
    id_jabatan character varying(32),
    id_jenis_pekerjaan character varying(32),
    tipe_karyawan integer DEFAULT 0,
    status integer DEFAULT 0,
    update_at integer,
    update_date date,
    created_date timestamp without time zone,
    created_by integer
);
COMMENT ON COLUMN public.tbl_calon_karyawan.status IS '0 = CALON KARYAWAN,  1 = KARYAWAN';
COMMENT ON COLUMN public.tbl_calon_karyawan.status IS '0 = Tidak aktif 1 = Aktif';
COMMENT ON COLUMN public.tbl_calon_karyawan.jenis_kelamin IS 'P = PRIA W = WANITA';
COMMENT ON COLUMN public.tbl_calon_karyawan.agama IS '1 = ISLAM, 2 = KRISTEN, 3 = KATOLIK , 4 = BUDHA, 5= HINDU, 6 = KONGHUCHU';

    -- - id
    -- - nik
    -- - nama
    -- - jenis_kelamin
    -- - tempat_lahir
    -- - tanggal_lahir
    -- - agama
    -- - alamat
    -- - email
    -- - no_hp
    -- - id_jabatan
    -- - created_date
    -- - created_by

-- table_jabatan
--     - id
--     - nama_jabatan
--     - created_date
--     - created_by

CREATE TABLE table_kategori_soal(
    id character varying(32) DEFAULT uuid(),
    nama_kategori character varying(100),
    persentase numeric (4,2)
);

-- table_kategori_soal
--     - id
--     - nama_kategori
--     - persentase

CREATE TABLE tbl_soal(
    id character varying(32) DEFAULT uuid(),
    id_kategori character varying(32),
    soal character varying(100),
    created_date timestamp without time zone,
    created_by integer
);

-- table_soal
--     - id
--     - id_kategori
--     - soal
--     - created_date
--     - created_by

CREATE TABLE tbl_soal_detail(
    id character varying(32) DEFAULT uuid(),
    id_master character varying(32),
    jawaban_soal character varying(100),
    bobot_nilai integer,
    urutan integer,
    created_date timestamp without time zone,
    created_by integer 
);


-- table_soal_detail
--     - id
--     - id_master
--     - jawaban_soal
--     - bobot_nilai
--     - created_date
--     - created_by

CREATE TABLE tbl_hasil_jawaban(
    id character varying(32) DEFAULT uuid(),
    id_karyawan character varying(32),
    id_soal character varying(32),
    id_jawaban_detail character varying(32),
    created_date timestamp without time zone,
    created_by integer
);

-- table_hasil_jawaban
--     - id
--     - id_karyawan
--     - id_soal
--     - id_jawaban_detail
--     - created_date
--     - created_by
DROP table tbl_hasil_akhir;
CREATE TABLE tbl_hasil_akhir(
    id character varying(32) DEFAULT uuid(),
    id_hasil_jawaban character varying(32),
    total_nilai integer,
    rata_rata integer,
    grade character varying(10),
    keterangan text,
    created_date timestamp without time zone,
    created_by integer
);

-- table_hasil_akhir
--     - id
--     - id_hasil_jawaban
--     - total_nilai
--     - rata_rata
--     - grade
--     - keterangan
--     - created_date
--     - created_by

-- table new untuk TERUSAN SETELAH PEREKRUTAN KARYAWAN

CREATE TABLE tbl_company(
    id character varying(32) DEFAULT uuid(),
    company_code character varying(10),
    company_name character varying(100),
    address text,
    phone_number character varying(20),
    fax character varying(20),
    email character varying(100),
    npwp character varying(100),
    tanggal_pendirian date,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_branch(
    id character varying(32) DEFAULT uuid(),
    branch_code character varying(10),
    branch_name character varying(100),
    company_code character varying(10),
    address text,
    province character varying(10),
    city character varying(10),
    phone_number character varying(20),
    fax character varying(50),
    email character varying(100),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_departemen(
    id character varying(32) DEFAULT uuid(),
    kode character varying(10),
    nama character varying(30),
    pic character varying(50),
    bidang character varying(100),
    company_code character varying(10),
    branch_code character varying(10),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_posisi(
    id character varying(32) DEFAULT uuid(),
    kode_posisi character varying(10),
    nama_posisi character varying(100),
    kode_departemen character varying(10),
    created_date timestamp without time zone,
    created_by integer
);

drop table tbl_karyawan;
CREATE TABLE tbl_karyawan(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    kode_calon_karyawan character varying(10),
    nik character varying(20),
    nama character varying(100),
    jenis_kelamin character varying(1), -- P - PRIA -  W: WANITA
    tempat_lahir character varying(50),
    tanggal_lahir date,
    agama integer DEFAULT 0,
    alamat text,
    no_hp character varying(20),
    email character varying(50),
    kode_posisi character varying(32),
    id_jenis_pekerjaan character varying(32),
    tipe_karyawan integer DEFAULT 0,
    bank_account_no character varying(30),
    bank_code character varying(10),
    status integer DEFAULT 0,
    update_at integer,
    update_date date,
    created_date timestamp without time zone,
    created_by integer
);

COMMENT ON COLUMN public.tbl_karyawan.status IS '0 = CALON KARYAWAN,  1 = KARYAWAN';
COMMENT ON COLUMN public.tbl_karyawan.status IS '0 = Tidak aktif 1 = Aktif';
COMMENT ON COLUMN public.tbl_karyawan.jenis_kelamin IS 'P = PRIA W = WANITA';
COMMENT ON COLUMN public.tbl_karyawan.agama IS '1 = ISLAM, 2 = KRISTEN, 3 = KATOLIK , 4 = BUDHA, 5= HINDU, 6 = KONGHUCHU';

drop table tbl_karyawan_detail;
CREATE TABLE tbl_karyawan_detail(
    id character varying(32) DEFAULT uuid(),
    id_karyawan character varying(32),
    jenis_karyawan integer DEFAULT 0,
    join_date date,
    masa_kontrak integer,
    tgl_awal_kontrak date,
    tgl_akhir_kontrak date,
    resign_date date,
    created_date timestamp without time zone,
    created_by integer
);

COMMENT ON COLUMN public.tbl_karyawan_detail.jenis_karyawan IS '1 = KARYAWAN KONTRAK ,  2 = KARYAWAN TETAP , ';

drop table tbl_jenis_pekerjaan;
CREATE TABLE tbl_jenis_pekerjaan(
    id character varying(32) DEFAULT uuid(),
    jenis_pekerjaan character varying(100),
    status integer DEFAULT 0,
    from_date date,
    thru_date date,
    keterangan text,
    created_date timestamp without time zone,
    created_by integer
);

COMMENT ON COLUMN public.tbl_karyawan.status IS '0 = CLOSE , 1= OPEN';

drop table tbl_karyawan_log;
CREATE TABLE tbl_karyawan_log(
    id character varying(32) DEFAULT uuid(),
    id_karyawan character varying(32),
    status_old integer,
    status_new integer,
    status integer,
    data jsonb
);


CREATE TABLE tbl_benefit_karyawan(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    gaji_pokok numeric(18,0),
    tunjangan numeric(18,0),
    uang_makan numeric(18,0),
    bpjs numeric(18,0),
    bonus numeric(18,0),
    tunjangan_hari_raya numeric(18,0),
    gaji_13 numeric(18,0),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_bank(
    id character varying(32) DEFAULT uuid(),
    bank_code character varying(10),
    bank_name character varying(100),
    bank_code_transfer character varying(5),
    bank_branch character varying(10),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_karyawan_pemerintahan(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    npwp character varying(30),
    no_bpjs character varying(30),
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_education(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    tipe_sekolah integer DEFAULT 0,
    nama_sekolah character varying(50),
    alamat text,
    tahun_lulus character varying(4),
    created_date timestamp without time zone,
    created_by integer
);

COMMENT ON COLUMN public.tbl_karyawan.status IS '0 = TK , 1= SD ,2 = SMP, 3= SMA ,4 = D3 , 5= S1 , 6= S2, 7= S3';

CREATE TABLE tbl_family(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    nama_ayah character varying(50),
    tanggal_lahir_ayah date,
    alamat_ayah text,
    no_hp_ayah character varying(50),
    pekerjaan_ayah character varying(100),
    nama_ibu character varying(50),
    tanggal_lahir_ibu date,
    alamat_ibu text,
    no_hp_ibu character varying(50),
    pekerjaan_ibu character varying(100),
    nama_wali character varying(50),
    tanggal_lahir_wali date,
    alamat_wali text,
    no_hp_wali character varying(50),
    pekerjaan_wali character varying(100),
    created_date timestamp without time zone,
    create_by integer
);

CREATE TABLE tbl_karyawan_file(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    data jsonb,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_organisasi(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    kode_organisasi character varying(10),
    nama_organisasi character varying(100),
    alamat text,
    tanggal_bergabung date,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_promosi(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    kode_jabatan_old character varying(10),
    kode_jabatan_new character varying(10),
    data jsonb,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_pengalaman_kerja(
    id character varying(32) DEFAULT uuid(),
    kode_karyawan character varying(10),
    nama_perusahaan character varying(100),
    alamat text,
    data jsonb,
    created_date timestamp without time zone,
    create_by integer
);


CREATE TABLE tbl_profesi(
    id character varying DEFAULT uuid(),
    nama_profesi character varying(100),
    inisial character varying(10),
    keterangan text,
    created_date timestamp without time zone,
    created_by integer
);

CREATE TABLE tbl_keterangan_jawaban(
    id character varying(32) DEFAULT uuid(),
    kode_jawaban character varying(100),
    keterangan text,
    saran text,
    data jsonb
);

CREATE TABLE tbl_keterangan_jawaban_karyawan(
    id character varying(32) DEFAULT uuid(),
    id_karyawan character varying(32),
    id_keterangan_jawaban character varying(32),
    id_hasil_akhir character varying(32),
    hasil_jawaban character varying(10),
    keterangan text,
    created_date timestamp without time zone,
    create_by integer
);
-- end table new untuk terusan setelah perekrutan karyawan