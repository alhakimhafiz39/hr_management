<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function api_post_data($url,$fields)
{
	$field_string = http_build_query($fields);
	// echo $field_string;
	// die();

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($field_string));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
	//curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
	//curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);

	//execute post
	$content = curl_exec($ch);
	//close connection
	curl_close($ch);
	// echo $content;
	// die();
	$ret = json_decode($content);
	return $ret;
}
function api_jqgrid_data($url,$fields)
{
	$field_string = http_build_query($fields);

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($field_string));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
	//curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
	//curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);

	//execute post
	$content = curl_exec($ch);
	//close connection
	curl_close($ch);
	
	echo $content;
}

function getJSONData($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

	$data = curl_exec($ch); // execute curl request
	curl_close($ch);

	return $data;
}
