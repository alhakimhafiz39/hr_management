<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	private $_salt = 'hrmanagement2020';

	public function __construct()
	{
		parent::__construct(true);
		$this->load->model('model_core');
	}

	public function index()
	{
		$data['title'] = 'Login';
		//restrict users to go back to login if session has been set null
		$this->load->view('login',$data);
	}

	public function auth()
	{	
		$response = array('status' => false);
		$username = $this->input->post('username');
		$password = sha1($this->input->post('password').$this->_salt);
		$last_login = date('Y-m-d H:i:s');
		
		$cek = $this->model_core->authentication($username,$password);
		if($cek==true){
			$user_id = $cek['user_id'];
			$cek['is_logged_in'] = true;
			$this->session->set_userdata($cek);
			$this->db->query("UPDATE tbl_user set last_login=? where user_id = ?",array($last_login,$user_id));
			$response['status'] = true;
			$response['message'] = 'Login Successfully!, Please wait...';
		}else{
			redirect('login/dashboard','refresh');
		}
		echo json_encode($response);
	}
	
}

