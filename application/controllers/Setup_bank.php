<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_bank extends MY_Controller {

	public function __construct()
	{
        parent::__construct(true);
        $this->load->model('model_setup_bank');
	}

	public function index()
	{
		$this->template->set('title', 'Setup Bank');
        $this->template->set('nav', 'Setup Bank');
        $this->template->set('nav_list', array('Home', 'System', 'Setup Bank'));
        $this->template->load_main('setup_bank/bank');
    }
    
    public function jqgrid_bank()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';
		$keyword = isset($_REQUEST['keyword'])?$_REQUEST['keyword']:'';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_setup_bank->jqgrid_bank('','','','',$keyword);

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_setup_bank->jqgrid_bank($sidx,$sord,$limit_rows,$start,$keyword);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['bank_code']
                ,$row['bank_name']
                ,$row['bank_code_transfer']
			);
			$i++;
		}

		echo json_encode($responce);
	}
    
    function do_save_bank(){
		$bank_code = $this->input->post('bank_code');
		$bank_name = $this->input->post('bank_name');
		$bank_code_transfer = $this->input->post('bank_code_transfer');
		$created_date = date('Y-m-d H:i:s');
		$created_by = $this->session->userdata('user_id');

		$bValid = true;
		$message = 'An error occurred, please try again later';

		$data = array(
			'bank_code'=>$bank_code,
			'bank_name'=>$bank_name,
			'bank_code_transfer'=>$bank_code_transfer,
			'created_date'=>$created_date,
			'created_by'=>$created_by
		);

		if($bValid==true){
			$db = $this->db;
			$db->trans_begin();
			$db->insert('tbl_bank',$data);
			if($db->trans_status()===true){
				$db->trans_commit();
				$return = array('success'=>true,'message'=>'Data Successfully Saved !');
			}else{
				$db->trans_rollback();
				$return = array('success'=>false,'message'=>$message);
			}
		}else{
			$return = array('success'=>false,'message'=>$message);
		}

		echo json_encode($return);
	}
	
	function get_data_bank_by_id(){
		$id = $this->input->post('id');
		$data = $this->model_setup_bank->get_data_bank_by_id($id);
		echo json_encode($data);
	}

    function do_update_bank(){
        $id_bank = $this->input->post('id_bank');
        $bank_code = $this->input->post('bank_code');
		$bank_name = $this->input->post('bank_name');
		$bank_code_transfer = $this->input->post('bank_code_transfer');

		$bValid = true;
		$message = 'An error occurred, please try again later';

		$data = array(
			'bank_code'=>$bank_code,
			'bank_name'=>$bank_name,
			'bank_code_transfer'=>$bank_code_transfer
		);
		$param = array('id'=>$id_bank);
		if($bValid==true){
			$db = $this->db;
			$db->trans_begin();
			$db->update('tbl_bank',$data,$param);
			if($db->trans_status()===true){
				$db->trans_commit();
				$return = array('success'=>true,'message'=>'Data Successfully Saved !');
			}else{
				$db->trans_rollback();
				$return = array('success'=>false,'message'=>$message);
			}
		}else{
			$return = array('success'=>false,'message'=>$message);
		}

		echo json_encode($return);
	}

	function do_delete_bank(){
		$id = $this->input->post('id');
		$param = array('id'=>$id);
		$db = $this->db;
		$db->trans_begin();
		$db->delete('tbl_bank',$param);
		if($db->trans_status()===true){
			$db->trans_commit();
			$return = array('success'=>true);
		}else{
			$db->trans_rollback();
			$return = array('success'=>false);
		}
		echo json_encode($return);
	}
}