<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Login | <?php echo $this->config->item('app_name'); ?></title>

	<!-- Icon -->
    <!-- <link rel="icon" href="<?= base_url('assets/img/logo/ico-chubb.png') ?>" /> -->

	<!-- General CSS Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" crossorigin="anonymous">

	<!-- CSS Libraries -->

	<!-- Template CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css">

	<!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">
</head>

<body>
	<div id="app">
		<section class="section">
			<div class="container mt-5">
				<div class="row">
					<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
						<div class="login-brand">
							<h4>HR Management</h4> System
						</div>

						<div class="card card-primary">
							<div class="card-header"><h4>Login</h4></div>

							<div class="card-body">
								<form method="POST" action="#" id="login" class="needs-validation" novalidate="">
									<div class="alert alert-danger" id="responseDiv" style="display:none;">
										<button type="button" class="close" data-close="alert" id="clearMsg"><span aria-hidden="true">&times;</span></button>
										<span id="message"></span>
									</div>

									<div class="form-group">
										<label for="username">Username</label>
										<input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Please fill in your username
										</div>
									</div>

									<div class="form-group">
										<div class="d-block">
											<label for="password" class="control-label">Password</label>
										</div>
										<input id="password" type="password" class="form-control" name="password" tabindex="2" required>
										<div class="invalid-feedback">
											please fill in your password
										</div>
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-info btn-lg btn-block" tabindex="3" id="btn-login">
											Login
										</button>
									</div>
								</form>

							</div>
						</div>
						<div class="simple-footer">
							Copyright &copy; <?php echo $this->config->item('app_company_name') ?>
						</div>
					</div>
				</div>					
			</div>
		</section>
	</div>

	<!-- General JS Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/stisla.js"></script>

	<!-- JS Libraies -->
	<script>
		// var x = document.getElementById("demo");
		getLocation();

		function getLocation() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(showPosition);
			} else { 
				// x.innerHTML = "Geolocation is not supported by this browser.";
				alert("Geolocation is not supported by this browser.");
			}
		}

		function showPosition(position) {
			// x.innerHTML = "Latitude: " + position.coords.latitude + 
			// "<br>Longitude: " + position.coords.longitude;
			console.log(position, 'position');
			
		}

		$('#login').submit(function(e){
			e.preventDefault();
			$('#loading').fadeIn();
			$("#btn-login").html('<i class="fas fa-spinner fa-pulse"></i>');
			$("#login :input").attr("readonly", true);

			var user = $('#login').serialize();
			var login = function(){
				$.ajax({
					url: '<?php echo site_url('login/auth') ?>',
					type: 'POST',
					timeout: 1000,
					dataType: 'json',
					data: user
				})
				.done(function(response) {
					// console.log("success", response);
					$('#message').html(response.message);
					$('#logText').html('Login');
					if(response.status == true){
						$('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
						setTimeout(function(){
							window.location = "<?php echo site_url('home/dashboard') ?>";
							$("#login :input").attr("readonly", false);
						}, 3000);
					}else{
						$('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
						$("#login :input").attr("readonly", false);
						$("#btn-login").html('Login');
					}
				})
				.fail(function(response) {
					$('#message').html("Opps, something wrong please try again or contact administrator");
					$('#responseDiv').removeClass('alert-danger').addClass('alert-danger').show();
					$('#login')[0].reset();
					$("#login :input").attr("readonly", false);
					$("#btn-login").html('Login');
				})
				.always(function(response) {
					// console.log("complete", response);
				});
			};
			setTimeout(login, 3000);
		});

		$(document).on('click', '#clearMsg', function(){
			$('#responseDiv').hide();
		});
		
	</script>

	<!-- Template JS File -->
	<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>

	<!-- Page Specific JS File -->
</body>

</html>
