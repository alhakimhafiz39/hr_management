<div class="card">
	<div class="card-header">
		<h4>STEP 1 OF 3</h4>
	</div>
	<div class="card-body">
		<form action="<?php echo site_url('ilustrasi/ilustrasi_danasiswa'); ?>" id="submit_form" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
			<input type="hidden" name="id_ilustrasi" id="id_ilustrasi" readonly="" value="">
			<div class="alert alert-danger" style="display:none;">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success" style="display:none;">
				<button class="close" data-dismiss="alert"></button>
				Successfully saved
			</div>
			<div id="smartwizard">
				<ul>
					<li><a href="#step-1">Data Peserta<br /><small></small></a></li>
					<li><a href="#step-2">Rencana Penarikan Sebagian NT<br /><small></small></a></li>
					<li><a href="#step-3">Ilustrasi<br /><small></small></a></li>
					<!-- <li><a href="#step-4">Selesai<br /><small></small></a></li> -->
				</ul>
				<div>
					<div id="step-1" style="padding:20px !important;">

						<!-- BEGIN DATA PESERTA -->
						<?php $this->load->view('soal/data_peserta') ?>
						<!-- END DATA PESERTA -->

					</div>
					<div id="step-2" style="padding:20px !important;">
						
						<!-- BEGIN DATA RENCANA PENDIDIKAN -->
						<?php $this->load->view('soal/soal_main') ?>
						<!-- END DATA RENCANA PENDIIKAN -->

					</div>
					<div id="step-3" style="padding:20px !important;">
					
						<!-- BEGIN DATA RENCANA PENDIDIKAN -->
						<?php $this->load->view('soal/ilustrasi') ?>
						<!-- END DATA RENCANA PENDIIKAN -->

					</div>
					<!-- <div id="step-4" style="padding:20px !important;"> -->

						<!-- <div id="form-step-3" role="form" data-toggle="validator"> -->
							<!-- BEGIN DATA RENCANA PENDIDIKAN -->
							<?php echo ''; //$this->load->view('ilustrasi/danasiswa/finish') ?>
							<!-- END DATA RENCANA PENDIIKAN -->
						<!-- </div> -->

					<!-- </div> -->
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		// Toolbar extra buttons
		var btnDownload = $('<butto id="btn-download" style="display:none"><i class="fa fa-download"></i></button>').text('Download Ilustrasi')
			.addClass('btn btn-danger btn-icon icon-left')
			.append("&nbsp;<i class='fa fa-download'></i>")
			.on('click', function () {
				// if (!$(this).hasClass('disabled')) {
				// 	var elmForm = $("#submit_form");
				// 	if (elmForm) {
				// 		elmForm.validator('validate');
				// 		var elmErr = elmForm.find('.has-error');
				// 		if (elmErr && elmErr.length > 0) {
				// 			APP.swalInfo('Oops we still have error in the form');
				// 			return false;
				// 		} else {
				// 			// APP.swalInfo('Great! we are ready to submit form');
				// 			var id_ilustrasi = $('#id_ilustrasi').val();
				// 			// alert(id_ilustrasi);
				// 			window.open(site_url+'ilustrasi/prevdanasiswa/'+id_ilustrasi+'/download');
				// 			setTimeout(() => {
				// 				window.location = "<?php echo site_url('ilustrasi/danasiswa') ?>";
				// 			}, 1500);
				// 			return false;
				// 		}
				// 	}
				// }
			});

		var btnPrint = $('<butto id="btn-cetak" style="display:none"></button>').text('Cetak Ilustrasi')
			.addClass('btn btn-warning btn-icon icon-left')
			.append("&nbsp;<i class='fa fa-print'></i>")
			.on('click', function () {
				// if (!$(this).hasClass('disabled')) {
				// 	var elmForm = $("#submit_form");
				// 	if (elmForm) {
				// 		elmForm.validator('validate');
				// 		var elmErr = elmForm.find('.has-error');
				// 		if (elmErr && elmErr.length > 0) {
				// 			APP.swalInfo('Oops we still have error in the form');
				// 			return false;
				// 		} else {
				// 			$("#btn-cetak").hide();
				// 			$("#btn-download").hide();
				// 			$('#form-step-2').printElement();
				// 			// var options = { mode : "iframe" };
				// 			// $("#form-step-2").print(); //OPTIONAL
				// 			$("#btn-cetak").hide();
				// 			$("#btn-download").show();
				// 		}
				// 	}
				// }
			});

		// var btnCancel = $('<button></button>').text('Cancel')
		// 	.addClass('btn btn-danger')
		// 	.on('click', function () {
		// 		$('#smartwizard').smartWizard("reset");
		// 		$('#submit_form').find("input, textarea, select").val("");
		// 	});

		// Smart Wizard
		$('#smartwizard').smartWizard({
			selected: 0,
			theme: 'arrows',
			transitionEffect: 'fade',
			transitionSpeed: '400',
			// lang: { next: "Selanjutnya", previous: "Kembali" },
			transitionEffect: 'fade',
			toolbarSettings: {
				toolbarPosition: 'bottom',
				toolbarExtraButtons: [btnPrint, btnDownload]
			},
			anchorSettings: {
				anchorClickable: false, // Enable/Disable anchor navigation
                enableAllAnchors: false, // Activates all anchors clickable all times
                markDoneStep: false, // add done css
                enableAnchorOnDoneStep: false // Enable/Disable the done steps navigation
			},
			useURLhash: true
		});

		$("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
			var elmForm = $("#form-step-" + stepNumber);
			// stepDirection === 'forward' :- this condition allows to do the form validation
			// only on forward navigation, that makes easy navigation on backwards still do the validation when going next
			if (stepDirection === 'forward' && elmForm) {
				elmForm.validator('validate');
				var elmErr = elmForm.children('.has-error');
				if (elmErr && elmErr.length > 0) {
					// Form validation failed
					return false;
				}
			}
			return true;
		});

		$("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {
			// Enable finish button only on last step
			if (stepNumber == 2) {
				$('.btn-cetak').removeClass('disabled');
				$('#btn-cetak').hide();
				$('#btn-download').show();
			} else {
				$('.btn-cetak').addClass('disabled');
				$('#btn-cetak').hide();
				$('#btn-download').hide();
			}
			// console.log(stepNumber, 'stepNumber');

			var form = $('#submit_form');

			if (form.valid() == false) {
				return false;
			}

			if (stepNumber == 2) {
				var nama_peserta = $("#nama_peserta").val();
				var tanggal_lahir = $("#tanggal_lahir").val();
				var usia = $("#usia").val();
				var usia_anak = $("#usia_anak").val();
				var masa_perjanjian = $("#masa_perjanjian").val();
				var periode_bayar = $("#periode_bayar").val();
				var rate_investasi = $("#rate_investasi").val()*100;
				var kontribusi_dibayarkan = $("#kontribusi_dibayarkan").val();
				var masa_bayar_kontribusi = $("#masa_bayar_kontribusi").val();
				var tot_kontribusi = $("#tot_kontribusi").val();
				var dana_pendidikan = $("#dana_pendidikan").val();
				var total_dana_kebajikan_ti = $("#dana_kebajikan_ti").val();
				var total_dana_kebajikan_pa = $("#total_dana_kebajikan_pa").val();
				var total_dana_kebajikan_tpd = $("#total_dana_kebajikan_tpd").val();
				var total_dana_kebajikan_ci = $("#total_dana_kebajikan_ci").val();	                    	
				var rider_manfaat = $("#rider_manfaat").val();

				if(periode_bayar=='1'){
					periode_bayar = "Bulanan";
				}else if(periode_bayar=='2'){
					periode_bayar = "Triwulanan";
				}else if(periode_bayar=='3'){
					periode_bayar = "Semesteran";
				}else if(periode_bayar=='4'){
					periode_bayar = "Tahunan";
				}else if(periode_bayar=='5'){
					periode_bayar = "Sekaligus";
				}else{
					periode_bayar = "-";
				}
				rider_tpd = '';
				rider_ci = '';
																			
				if($("#rider_tpd").is(":checked")){ 
					rider_tpd = "Cacat Tetap Total ";
					
				}
				if($("#rider_ci").is(":checked")){ rider_ci = ", Penyakit Kritis ";}

				var Vrider_manfaat = rider_tpd+rider_ci;

				$("#Vnama_peserta").html(" : "+nama_peserta);
				$("#Vtanggal_lahir").html(" : "+tanggal_lahir);
				$("#Vusia").html(" : "+usia+" Tahun");
				$("#Vusia_anak").html(" : "+usia_anak+" Tahun");
				$("#Vmasa_perjanjian").html(" : "+masa_perjanjian+" Tahun");
				$("#Vperiode_bayar").html(" : "+periode_bayar);
				$("#Vrate_investasi").html(" : "+rate_investasi+" %");
				$("#Vkontribusi_dibayarkan").html(" : "+kontribusi_dibayarkan);
				$("#Vmasa_bayar_kontribusi").html(" : "+masa_bayar_kontribusi+" Tahun");
				$("#Vtot_kontribusi").html(" : "+tot_kontribusi);
				$("#Vdana_pendidikan").html(" : "+dana_pendidikan);
				$("#Vtotal_dana_kebajikan_ti").html(" : "+total_dana_kebajikan_ti);
				$("#Vrider_manfaat").html(" : "+Vrider_manfaat);
				$.ajax({
					type:"POST",dataType:"json",data:{
						carbay:$("#periode_bayar").val(),
						product_code:'50022'
					},
					url:site_url+'ilustrasi/getProdukUjroh',
					async:false,
					success:function(response) {
						$("#Vtable_ujroh").html(response.html);
						form.trigger('submit');
					},
					error: function(response){
						console.log("Failed to Connect into Database, Please Contact Your Administrator!")
					}
				})
			}
			

		});

		// Ilustrasi 
		// #Data Peserta

		$('#tanggal_lahir').on('apply.daterangepicker', function(ev, picker) {
			if ($(this).val().replace(/\_/g,'').replace(/\//g,'').length==8) {
				getUsiaAsuransi($(this).val(),'usia');	                		                	
			}
		});

		$('#tanggal_lahir').on('change.daterangepicker', function(ev, picker) {
			if ($(this).val().replace(/\_/g,'').replace(/\//g,'').length==8) {
				getUsiaAsuransi($(this).val(),'usia');	                		                	
			}
		});

		$('#tanggal_lahir_anak').on('change.daterangepicker', function(ev, picker) {
			if ($(this).val().replace(/\_/g,'').replace(/\//g,'').length==8) {
				getUsiaAsuransi($(this).val(),'usia_anak');
				var usia_anak = $("#usia_anak").val();
				if (usia_anak > 0) {
					var masa_perjanjian = 22 - usia_anak;
					$("#masa_perjanjian").val(masa_perjanjian);	
				}else{
					Metronic.WarningAlert('Minimal Usia Penerima Hibah 1 Tahun !');
					$(this).val('');
				}
			}
		});

		$('#tanggal_lahir_anak').on('apply.daterangepicker', function(ev, picker) {
			if ($(this).val().replace(/\_/g,'').replace(/\//g,'').length==8) {
				getUsiaAsuransi($(this).val(),'usia_anak');
				var usia_anak = $("#usia_anak").val();
				if (usia_anak > 0) {
					var masa_perjanjian = 22 - usia_anak;
					$("#masa_perjanjian").val(masa_perjanjian);	
				}else{
					Metronic.WarningAlert('Minimal Usia Penerima Hibah 1 Tahun !');
					$(this).val('');
				}
			}
		});

		var getUsiaAsuransi = function(date,obj) {

			if (date!="") {
				var xhr;

				if(xhr && xhr.readyState != 4){
					xhr.abort();
				}

				xhr = $.ajax({
					type:"POST",
					dataType:"json",
					data:{
						date:date
					},
					url:site_url+'ilustrasi/getUsiaAsuransi',
					async:false,
					success:function(response) {
						//console.log(response.usia);
						var a = "#"+obj;
						$(a).val(response.usia);
						//var vUsia = response.usia;
						//var masa_perjanjian = 80-response.usia;
						//$('#masa_perjanjian').val(masa_perjanjian);
					},
					error: function(response){
						console.log("Failed to Connect into Database, Please Contact Your Administrator!")
					}
				})
			}
			//return vUsia;
		}

		var calc_parameter = function() {
			var rdp = APP.ConvertNumeric($("#dana_pendidikan").val());
			var masa_bayar_kontribusi = APP.ConvertNumeric($("#masa_bayar_kontribusi").val());
			var periode_bayar = $("#periode_bayar").val();
			switch(periode_bayar) {
				case "1":
					var fk = 12;
					var min = 200000;
				break;
				case "2":
					var fk = 4;
					var min = 300000;
				break;
				case "3":
					var fk = 2;
					var min = 500000;
				break;
				case "4":
					var fk = 1;
					var min = 1000000;
				break;
				case "5":
					var fk = 1;
					var min = 10000000;
				break;
			}
			var kontribusi = rdp/(masa_bayar_kontribusi*fk);
			var persen_tambahan = APP.ConvertNumeric($("#tambahan_kontribusi").val());
			if (persen_tambahan!=0) {
				kontribusi = kontribusi*(1+(persen_tambahan/100));
			}
			var tot_kontribusi = kontribusi * masa_bayar_kontribusi * fk;
			console.log('kontribusi nya adalah ',kontribusi);
			if(parseFloat(kontribusi)!=0 || parseFloat(rdp)!=''){
				minimal = parseFloat(min)
				console.log('minimal adalah ',minimal)
				if(kontribusi<parseFloat(min)){
					APP.swalWarning('Minimal Kontribusi Di Bayarkan Tidak Sesuai Ketentuan !');
					$("#dana_pendidikan").val('');
					$("#kontribusi_dibayarkan").val('');
					$("#tot_kontribusi").val('');
					$("#dana_kebajikan_ti").val('');
					$("#dana_kebajikan_pa").val('');
				}else{
					$("#kontribusi_dibayarkan").val(APP.NumberFormat(kontribusi,0,',','.'));
					$("#tot_kontribusi").val(APP.NumberFormat(tot_kontribusi,0,',','.'));
					var dk_ti = rdp/2;
					$("#dana_kebajikan_ti").val(APP.NumberFormat(dk_ti,0,',','.'));
					$("#dana_kebajikan_pa").val(APP.NumberFormat(rdp,0,',','.'));
				}
			}
		}

		$('#dana_pendidikan').change(function(){
			calc_parameter();	            	
		})

		$("#tambahan_kontribusi").change(function() {
			calc_parameter();		            
		})

		$("#masa_bayar_kontribusi").change(function(){
			calc_parameter();
		})

		$("#periode_bayar").change(function(){
			calc_parameter();
			var periode_bayar = $(this).val();
			if(periode_bayar=='5'){//sekaligus
				$("#dana_pendidikan").val('0');
				$("#dana_pendidikan").attr('readonly',false);
				$("#masa_bayar_kontribusi").val('1');
				$("#masa_bayar_kontribusi").attr('readonly',true);
				$("#v_manfaat_tambahan").hide();
			}else{
				$("#dana_pendidikan").val('0');
				$("#dana_pendidikan").attr('readonly',false);
				$("#masa_bayar_kontribusi").attr('readonly',false);
				$("#v_manfaat_tambahan").show();
			}
		})

		$("#dana_pendidikan").change(function(){
			var periode_bayar = $("#periode_bayar").val();
			var dana_pendidikan = parseFloat(APP.ConvertNumeric($(this).val()));
			switch(periode_bayar){
				case "1":
					carbay = "bulanan";
				break;
				case "2":
					carbay = "triwulanan";
				break;
				case "3":
					carbay = "semesteran";
				break;
				case "4":
					carbay = "tahunan";
				break;
				case "5":
					carbay = "sekaligus";
				break;
				default:
					carbay = "-";
				break;
			}
			if(periode_bayar=='5'){
				if(dana_pendidikan<parseFloat(30000000)){
					APP.swalWarning("Minimal rencana dana pendidikan untuk cara bayar "+carbay+" adalah 30.000.000");
					$("#dana_pendidikan").val('30000000');
					calc_parameter();
				}
			}else{
				if(dana_pendidikan<parseFloat(15000000)){
					APP.swalWarning("Minimal rencana dana pendidikan untuk cara bayar "+carbay+" adalah 15.000.000");
					$("#dana_pendidikan").val('15000000');
					calc_parameter();
				}
			}
		})

		// #END Data Peserta
		// #BEGIN - Rencana Penarikan Sebagian NT
		$(document).on('click', 'a#yes', function () {
			html = `<tr>
						<td align="center">
							<input type="text" class="form-control" name="tahun[]" id="tahun" placeholder="0" style="width:80px;">
						</td>
						<td align="center">
							<input type="text" class="form-control" name="persentase[]" id="persentase" placeholder="0" style="width:100px;">
						</td>
						<td align="center">
							<a class="btn btn-sm btn-primary" id="yes" style="color: #fff !important; cursor: pointer;">
                                <i class="fas fa-plus-circle"></i>
                            </a>
                            <a class="btn btn-sm btn-danger" id="no" style="color: #fff !important; cursor: pointer;">
                                <i class="fas fa-trash-alt"></i>
                            </a>
						</td>
					</tr>`;
			$(this).parent().parent().after(html);
		});

		$(document).on('click', 'a#no', function () {
			var rowCount = $('#tbl-rencanan-penarikan-sebagian-nt tr').length;			
			if (rowCount > 2) {
				$(this).parent().parent().remove();
			}else{
				APP.swalInfo("Can't to remove all");
			}
		});
		// END - Rencana Penarikan Sebagian NT

		var form = $('#submit_form');
		var error = $('.alert-danger', form);
		var success = $('.alert-success', form);

		form.validate({
			doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
			errorElement: 'span', //default input error message container
			errorClass: 'help-block help-block-error', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				nik: {
					required: true
				},
				// tanggal_lahir: {
				// 	required: true
				// },
				// usia: {
				// 	required: true
				// },
				// tanggal_lahir_anak: {
				// 	required: true
				// },
				// usia_anak: {
				// 	required: true
				// },
				// masa_perjanjian: {
				// 	required: true
				// },
				// periode_bayar: {
				// 	required: true
				// },
				// kontribusi_dibayarkan: {
				// 	required: true
				// },
				// masa_bayar_kontribusi: {
				// 	required: true
				// },
				// tot_kontribusi: {
				// 	required: true
				// },
				// dana_kebajikan_ti: {
				// 	required: true
				// },
				// dana_kebajikan_pa: {
				// 	required: true
				// }
			},

			errorPlacement: function (error, element) { 
			},

			invalidHandler: function (event, validator) { //display error alert on form submit   
				success.hide();
				error.show();
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
					.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {

			},

			submitHandler: function (form) {
				Metronic.blockUI();

				success.hide();
				error.hide();
				$(form).ajaxSubmit({
					dataType: 'json',
					success: function(response) {
						if (response.length != undefined || response.length!=null) {

							var n = response.length;
							n = n-1;
							var id_ilustrasi = response[n].id_ilustrasi;
							var persen_ujroh_thn1 = APP.NumberFormat(response[n].persen_ujroh_thn1,2,',','.');
							var persen_ujroh_thn2 = APP.NumberFormat(response[n].persen_ujroh_thn2,2,',','.');
							var persen_ujroh_thn3 = APP.NumberFormat(response[n].persen_ujroh_thn3,2,',','.');
							var persen_ujroh_thn4 = APP.NumberFormat(response[n].persen_ujroh_thn4,2,',','.');
							var status_underwriting = response[n].status_underwriting;
							$("#id_ilustrasi").val(id_ilustrasi);
							$("#persen_ujroh_thn1").html(persen_ujroh_thn1);
							$("#persen_ujroh_thn2").html(persen_ujroh_thn2);
							$("#persen_ujroh_thn3").html(persen_ujroh_thn3);
							$("#persen_ujroh_thn4").html(persen_ujroh_thn4);
							$("#status_underwriting").html(" : "+status_underwriting);
							$("#Vno_ilustrasi").html(" : "+response[n].no_ilustrasi);
							
							// Alert BEGIN
							if (response[n].status_response) {
								APP.swalSuccess();
							}
							// Alert END

							html = '';
							nomor = 1; 
							// alert(response.length);
							for(i = 0 ; i < (response.length-1) ; i++)
							{
								html += ' \
									<tr>\
										<td align="center" style="padding:3px !important;">\
											'+response[i].year_to+'\
										</td>\
										<td align="center" style="padding:3px !important;">\
											'+response[i].age+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].kontribusi_year)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].kontribusi_accumulation)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].dana_kebajikan)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].tabarru_accumulation)+'\
										</td>\
										<td align="center" style="padding:3px !important;">\
											'+response[i].tahapan_jenjang+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+response[i].tahapan_persen+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].tahapan_amount)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].dana_awal)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].nilai_tunai)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].dana_kebajikan_anak)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].dana_kebajikan_non_kec)+'\
										</td>\
										<td align="right" style="padding:3px !important;">\
											'+APP.NumberFormat(response[i].dana_kebajikan_kec)+'\
										</td>\
									</tr>\
								';
							}
							$("#preview_result tbody").html(html);	
						}else{
							APP.swalError();
						}			
						Metronic.unblockUI();		
					}
					,error:function(){
						APP.swalWarning('Perhitungan error ! Mohon periksa parameter inputan');
						Metronic.unblockUI();
					}
				});
			}

		});

	});

</script>
