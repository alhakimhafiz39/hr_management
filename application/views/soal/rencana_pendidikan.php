<div class="row" id="form-step-1" role="form" data-toggle="validator">
    <div class="col-md-6 offset-md-3">
        <table class="table table-hover table-responsive" id="tbl-rencanan-penarikan-sebagian-nt">
            <thead>
                <tr>
                    <th style="background:#DFBA49;">
                        Tahun
                    </th>
                    <th style="background:#DFBA49;">
                        Persentase
                    </th>
                    <th style="background:#DFBA49;">
                        + / x
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <input type="text" class="form-control currency" name="tahun[]" id="tahun" placeholder="0" style="width:80px;">
                    </td>
                    <td align="center">
                        <input type="text" class="form-control currency" name="persentase[]" id="persentase" placeholder="0" style="width:100px;">
                    </td>
                    <td align="center">
                        <a class="btn btn-sm btn-primary" id="yes" style="color: #fff !important; cursor: pointer;">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                        <a class="btn btn-sm btn-danger" id="no" style="color: #fff !important; cursor: pointer;">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
