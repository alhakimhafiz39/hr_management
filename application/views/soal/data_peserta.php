<form action="<?php echo site_url('biodata/add') ?>" method="post" enctype="multipart/form-data">
<div class="form-horizontal" id="form-step-0" role="form" data-toggle="validator">
	
	<div class="form-group row mb-4">
		<label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">NIK</label>
		<div class="col-lg-4 col-md-6">
			<input type="text" placeholder="Nik" class="form-control" name="nik" id="nik" required>
			<div class="help-block with-errors text-red"></div>
		</div>
	</div>

  <div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Nama</label>
    <div class="col-lg-4 col-md-6">
      <input type="text" name="nama" id="nama" placeholder="Nama " class="form-control" required>
      <div class="help-block with-errors text-red"></div>
    </div>
  </div>

  <div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Tempat Laahir</label>
    <div class="col-lg-4 col-md-6">
      <input type="text" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir " class="form-control" required>
      <div class="help-block with-errors text-red"></div>
    </div>
  </div>

  <div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Tanggal Lahir</label>
    <div class="col-lg-4 col-md-6">
      <input type="date" name="tgl" id="tgl" placeholder="Tanggal Lahir" class="form-control" required>
      <div class="help-block with-errors text-red"></div>
    </div>
  </div>

  <div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Jenis Kelamin</label>
    <div class="col-lg-4 col-md-6">
      <input type="radio" name="gender" value="P" required> Male<br>
      <input type="radio" name="gender" value="W" required> Female<br>
      <input type="radio" name="gender" value="other" required> Other
      <div class="help-block with-errors text-red"></div>
    </div>
  </div>

  <div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Agama</label>
    <div class="col-lg-4 col-md-6">
      <select class="form-control" name="agama" id="agama" required >
        <option>- PILIH -</option>
        <option value="1">Islam</option>
        <option value="2">Kristen</option>
        <option value="3">Katolik</option>
        <option value="4">Hindu</option>
        <option value="5">Budhha</option>
        <option value="6">Konghucu</option>
      </select>
    <div class="help-block with-errors text-red"></div>
  </div>
  </div>
	
	  <div class="form-group row mb-4">
      <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">Alamat</label>
      <div class="col-lg-4 col-md-6">
        <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat" required></textarea>
        <div class="help-block with-errors text-red"></div>
      </div>
    </div>
    <div class="form-group row mb-4">
      <label class="col-form-label text-md-right col-12 col-md-4 col-lg-4">No Hp</label>
      <div class="col-lg-4 col-md-6">
        <input type="text" name="nohp" id="nohp" placeholder="No HP" class="form-control" required>
        <div class="help-block with-errors text-red"></div>
      </div>
    </div>
	
</div>
</form>