<style>
	.table.table-md th,
	.table.table-md td {
		font-size: 11px !important;
        padding: 0px 5px 5px 5px !important;
        line-height: 19px !important;
    }
    
    li{
        font-size: 11px !important;
    }

</style>

<div id="form-step-2" role="form" data-toggle="validator">
    <div class="row">
        <div class="col-md-12">
            <table>
                <tr>
                    <td style="font-weight: bold;" width="620">
                        <h4>Ilustrasi Danasiswa </h4>
                    </td>
                    <td width="450" style="text-align: right;font-size:14px;">
                        <div>
                            <img src="<?php echo base_url().$this->config->item('app_logo_print') ?>" height="50">
                        </div>
                        Ilustrasi ini bukan merupakan bagian dari Kontrak Asuransi. Seluruh angka yang tertera dalam Ilustrasi ini hanya merupakan nilai asumi & tidak dijamin
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <h6>DATA PESERTA :</h6>
            <div class="table-responsive">
                <table class="table table-hover table-md">
                    <tbody>
                        <tr>
                            <td>NO ILUSTRASI</td>
                            <td class="text-left">
                                <div id="Vno_ilustrasi"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>NAMA PESERTA</td>
                            <td class="text-left">
                                <div id="Vnama_peserta"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>TANGGAL LAHIR</td>
                            <td class="text-left">
                                <div id="Vtanggal_lahir"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>USIA PESERTA</td>
                            <td class="text-left">
                                <div id="Vusia"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>USIA ANAK</td>
                            <td class="text-left">
                                <div id="Vusia_anak"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>MASA PERJANJIAN</td>
                            <td class="text-left">
                                <div id="Vmasa_perjanjian"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>AKUMULASI KONTRIBUSI</td>
                            <td class="text-left">
                                <div id="Vtot_kontribusi"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>RENCANA DANA PENDIDIKAN</td>
                            <td class="text-left">
                                <div id="Vdana_pendidikan"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>STATUS UNDERWRITING</td>
                            <td class="text-left">
                                <div id="status_underwriting"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <h6>PARAMETER ILUSTRASI :</h6>
            <div class="table-responsive">
                <table class="table table-hover table-md">
                    <tbody>
                        <tr>
                            <td>NO ILUSTRASI</td>
                            <td class="text-left">
                                <div id="Vno_ilustrasi"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">PERIODE BAYAR KONTRIBUSI</td>
                            <td valign="top">
                                <div id="Vperiode_bayar"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">RATE INVESTASI</td>
                            <td valign="top">
                                <div id="Vrate_investasi"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">KONTRIBUSI DIBAYARKAN</td>
                            <td valign="top">
                                <div id="Vkontribusi_dibayarkan"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">DANA KEBAJIKAN</td>
                            <td valign="top">
                                <div id="Vtotal_dana_kebajikan_ti"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">MASA BAYAR KONTRIBUSI</td>
                            <td valign="top">
                                <div id="Vmasa_bayar_kontribusi"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="200">MANFAAT TAMBAHAN BEBAS KONTRIBUSI</td>
                            <td valign="top">
                                <div id="Vrider_manfaat"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover table-md" id="preview_result">
                    <thead>
                        <tr>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                TAHUN
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                USIA
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                PEMBAYARAN
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                AKUMULASI KONTRIBUSI
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                DANA KEBAJIKAN
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                AKUM. TABARRU'
                            </th>
                            <!-- <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;" valign="middle" rowspan ="2">
                                                                                            AKUM. UJROH
                                                                                        </th> -->
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                JENJANG
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                %
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                TAHAPAN
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                DANA AWAL
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                NILAI TUNAI
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                DANA KEBAJIKAN ANAK
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                DANA KEBAJIKAN MENINGGAL NON.KEC
                            </th>
                            <th style="background:#DFBA49;vertical-align:middle;font-size:11px;padding:3px !important;"
                                valign="middle" rowspan="2">
                                DANA KEBAJIKAN MENINGGAL KEC.
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <u><strong>Tahapan Dana Pendidikan</strong></u>
        </div>
        <div class="col-md-3">
            <div class="table-responsive">
                <table class="table table-hover table-md">
                    <tr>
                        <td align="center" width="70">Jenjang</td>
                        <td align="center" width="50">%</td>
                        <td align="center">Keterangan</td>
                    </tr>
                    <tr align="center">
                        <td>TK A</td>
                        <td>5%</td>
                        <td rowspan="5">Prosentase (%)dari RDP</td>
                    </tr>
                    <tr align="center">
                        <td>TK B</td>
                        <td>10%</td>
                    </tr>
                    <tr align="center">
                        <td>SD</td>
                        <td>15%</td>
                    </tr>
                    <tr align="center">
                        <td>SMP</td>
                        <td>20%</td>
                    </tr>
                    <tr align="center">
                        <td>SMA</td>
                        <td>25%</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="table-responsive">
                <table class="table table-hover table-md">
                    <tr align="center">
                        <td width="65">PT</td>
                        <td width="50">%</td>
                        <td>Keterangan</td>
                    </tr>
                    <tr align="center">
                        <td>PT - 1</td>
                        <td>30,0%</td>
                        <td rowspan="5">Prosentase (%) dari Asumsi Nilai Tunai</td>
                    </tr>
                    <tr align="center">
                        <td>PT - 2</td>
                        <td>35,0%</td>
                    </tr>
                    <tr align="center">
                        <td>PT - 3</td>
                        <td>40,0%</td>
                    </tr>
                    <tr align="center">
                        <td>PT - 4</td>
                        <td>50,0%</td>
                    </tr>
                    <tr align="center">
                        <td>PT - 5</td>
                        <td>100%</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="table-responsive">
                <table class="table table-hover table-md">
                    <tr>
                        <td width="250" colspan="2"><b>BIAYA - BIAYA</b></td>
                    </tr>
                    <tr>
                        <td><b>UJROH AKUISISI</b></td>
                    </tr>
                    <tr>
                        <td>Tahun Ke I </td>
                        <td> <span id="persen_ujroh_thn1"></span> % Dari Kontribusi Peserta Tahun I</td>
                    </tr>
                    <tr>
                        <td>Tahun Ke II </td>
                        <td> <span id="persen_ujroh_thn2"></span> % Dari Kontribusi Peserta Tahun II</td>
                    </tr>
                    <tr>
                        <td>Tahun Ke III </td>
                        <td> <span id="persen_ujroh_thn3"></span> % Dari Kontribusi Peserta Tahun III</td>
                    </tr>
                    <tr>
                        <td>Tahun Ke IV </td>
                        <td> <span id="persen_ujroh_thn4"></span> % Dari Kontribusi Peserta Tahun IV dst</td>
                    </tr>
                    <tr style="padding-top:20px;">
                        <td style="padding-top:20px;"><b>Ujroh Wakalah Fee Investasi</b></td><td style="padding-top:20px;">: 3.00% dari Pokok tabungan peserta / Tahun</td>
                    </tr>
                    <tr>
                        <td><b>Biaya Admin</b></td><td> : 15.000 ,- / Bulan</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <strong>MANFAAT ASURANSI UTAMA</strong>
            <hr size="1">
                <ul>
                    <li>(1) Jika Peserta (Ayah dan Ibu) mengalami musibah meninggal dalam masa asuransi , maka:
                        <ul>
                            <li>Secara otomatis polis menjadi bebas Kontribusi dan <strong>Tahapan Dana Pendidikan</strong> tetap dibayarkan sesuai dengan jenjang pendidikan anak di masa mendatang hingga masa Perjanjian Asuransi berakhir;</li>
                            <li>Apabila Peserta meninggal bukan akibat kecelakaan , maka Penerima Manfaat akan menerima Dana Kebajikan sebesar 50% (Lima Puluh Persen) <strong>Rencana Dana</strong>Sebagai Dana Kebajikan;</li>
                            <li>
                                Apabila peserta meninggal akibat kecelakaan pada saat usia <= 60 tahun , maka Termaslahat akan menerima santunan duka sebesar dan 100% (seratus persen) <strong>Rencana Dana Pendidikan</strong> Sebagai Dana Kebajikan;
                            </li>
                        </ul>
                    </li>
                        <hr size="1">
                    <li>(2) Jika Anak (Putera - Puteri) sebagai Peserta mengalami musibah meninggal dalam masa perjanjian , maka Termaslahat akan menerima Dana Kebajikan sebesar Rp.15.000.000 (Lima Belas Juta Rupiah), ditambah Nilai Tunai polis pada saat Anak meninggal dan selanjutnya polis akan berakhir (Terminated);</li>
                    <hr>
                    <li>
                        (3) Jika Pemegang Polis peserta mengundurkan diri dalam masa perjanjian ,maka termaslahat akan menerima Nilai Tunai pada saat mengundurkan diri; 
                    </li>
                    <li>
                        (4) Pertanggungan polis akan berakhir , jika seluruh Tahapan Dana pendidikan telah diterima oleh termaslahat sesuai dengan jenjang pendidikan Anak.
                    </li>
                </ul>
                <br>
                <strong>MANFAAT TAMBAHAN <i>(Rider Benefit)</i></strong>
                <hr>
                <ul>
                    <li>
                        Jika Peserta (Ayah dan Ibu) mengalami musibah cacat tetap total (Total Permanent Disability), maka secara otomatis polis menjadi bebas kontribusi dan Tahapan Dana Pendidikan tetap dibayarkan sesuai dengan jenjang pendidikan anak di masa mendatang hingga masa perjanjian berakhir.
                    </li><hr>
                    <li>
                        Jika Peserta (Ayah dan Ibu) mengalami musibah atau menderita penyakit kritis dalam masa perjanjian , maka secara otomatis polis menjadi bebas kontribusi dan Tahapan Dana Pendidikan tetap dibayarkan sesuai dengan jenjang pendidikan anak di masa mendatang hingga masa perjanjian berakhir.
                    </li>
                </ul>
            <div style="float:right;">
                <p>User : <?php echo $this->session->userdata('fullname');?> | Date : <?php echo date("d-m-Y H:i:s");?></p>
            </div>
        </div>
    </div>
</div>