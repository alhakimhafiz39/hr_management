<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Login &mdash; <?php echo $this->config->item('app_name'); ?></title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" crossorigin="anonymous">

	<!-- CSS Libraries -->

	<!-- Template CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css">
</head>

<body>
	<div id="app">
		<section class="section">
			<div class="d-flex flex-wrap align-items-stretch">
				<div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
					<div class="p-4 m-3">
						<img src="<?php echo base_url().$this->config->item('app_logo'); ?>" alt="logo" width="80" class="shadow-light rounded-circle mb-5 mt-2">
						<h4 class="text-dark font-weight-normal">Welcome to <span class="font-weight-bold"><?php echo $this->config->item('app_name'); ?></span></h4>
						<p class="text-muted">Before you get started, you must login or register if you don't already have an account.</p>
						<form method="POST" action="<?php echo site_url('login/auth') ?>" class="needs-validation" novalidate="">
							<?php if ($this->session->flashdata('login_message')==true) { ?>
							<div class="alert alert-danger">
								<button class="close" data-close="alert"></button>
								<span><?php echo $this->session->flashdata('login_message')?></span>
							</div>
							<?php } ?>

							<div class="form-group">
								<label for="username">Username</label>
								<input id="username" type="username" class="form-control" name="username" tabindex="1" required
									autofocus>
								<div class="invalid-feedback">
									Please fill in your username
								</div>
							</div>

							<div class="form-group">
								<div class="d-block">
									<label for="password" class="control-label">Password</label>
								</div>
								<input id="password" type="password" class="form-control" name="password" tabindex="2" required>
								<div class="invalid-feedback">
									please fill in your password
								</div>
							</div>

							<div class="form-group text-right">
								<a href="#" class="float-left mt-3">
									Forgot Password?
								</a>
								<button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
									Login
								</button>
							</div>
						</form>

						<div class="text-center mt-5 text-small" style="margin-bottom:2px;">
							Copyright &copy; Salam Enterprise
						</div>
					</div>
				</div>
				<div
					class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom"
					data-background="<?php echo base_url() ?>assets/img/unsplash/login-bg.jpg">
					<div class="absolute-bottom-left index-2">
						<div class="text-light p-5 pb-2">
							<div class="mb-5 pb-3">
								<h1 class="mb-2 display-4 font-weight-bold"><?php echo greetings() ?></h1>
								<h5 class="font-weight-normal text-muted-transparent">Bali, Indonesia</h5>
							</div>
							Photo by <a class="text-light bb" target="_blank" href="https://unsplash.com/photos/a8lTjWJJgLA">Justin
								Kauffman</a> on <a class="text-light bb" target="_blank" href="https://unsplash.com">Unsplash</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- General JS Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/stisla.js"></script>

	<!-- JS Libraies -->
	<script>
		// var x = document.getElementById("demo");
		getLocation();

		function getLocation() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(showPosition);
			} else { 
				// x.innerHTML = "Geolocation is not supported by this browser.";
				alert("Geolocation is not supported by this browser.");
			}
		}

		function showPosition(position) {
			// x.innerHTML = "Latitude: " + position.coords.latitude + 
			// "<br>Longitude: " + position.coords.longitude;
			console.log(position, 'position');
			
		}
	</script>

	<!-- Template JS File -->
	<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>

	<!-- Page Specific JS File -->
</body>

</html>
